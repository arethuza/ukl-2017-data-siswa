<!DOCTYPE html>
<html>
<head>
	<title>Pendaftaran Siswa</title>
	<link rel="stylesheet" type="text/css" href="mainDaftar.css">
</head>
<body>
	<form action="in_daftar.php" method="post">
		<div id="formBox">
		<p>What are You Waiting for ?<span>Sign Up Now</span></p>
			<table>
				<tr>
					<td><p>Nomor Induk</p></td>
					<td><input type="text" name="induk_siswa" placeholder="ex: 91723897"></td>



					<td class="wow"><p>Nama Lengkap</p></td>
					<td><input type="text" name="nama_siswa" placeholder="ex: John Doe"></td>
				</tr>

				<tr>
					<td><p>Alamat</p></td>
					<td><textarea name="tempat_tinggal" placeholder="Address Bar"></textarea></td>
				
					<td class="wow"><p>Tempat Lahir</p></td>
					<td><textarea name="tempat_lahir" placeholder="Born Place"></textarea></td>
				</tr>

				<tr>
					<td><p>Agama</p></td>
					<td>
						<select name="agama">
						<option selected>Islam</option>
						<option>Kristen</option>.
						<option>Katholik</option>
						<option>Hindu</option>
						<option>Buddha</option>
						<option>Lainnya</option>
						</select>
					</td>
				
					<td class="wow"><p>Jenis Kelamin</p></td>
					<td>
						<select name="gender">
						<option>P</option>
						<option>L</option>
						</select>
					</td>
				</tr>

				<tr>
					<td><p>Nomor Ponsel</p></td>
					<td><input type="number" name="telp" placeholder="ex: 085288990835"></td>
				
					<td class="wow"><p>Tanggal Lahir</p></td>
					<td><input type="date" name="tanggal_lahir"></td>
				</tr>

				<tr>
					<td><p>Username</p></td>
					<td><input type="text" name="username_siswa" placeholder="Your Username"></td>
				
					<td class="wow"><p>Password</p></td>
					<td><input type="password" name="password"></td>
					<tr><a href="https://www.google.co.id/maps/place/Telkom+Schools:+SMK+Telkom+Malang/@-7.9768247,112.6567693,17z/data=!3m1!4b1!4m5!3m4!1s0x2dd6285c5c1b44e3:0xf6c889ac7452dc3a!8m2!3d-7.97683!4d112.658958?hl=en" target="_blank"><div id="fowto"></div></a><tr>
				</tr>
			</table>
			<hr>
			<input type="submit" name="daftar" value="DAFTAR">
			<p id="wowi">Already Have an Account ? Sign In&nbsp;<a href="login.php">Here</a></p>
		</div>
	</form>
</body>
</html>