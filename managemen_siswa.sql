-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 08, 2018 at 02:08 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `managemen_siswa`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_siswa`
--

CREATE TABLE `data_siswa` (
  `induk_siswa` varchar(50) NOT NULL,
  `nama_siswa` varchar(100) NOT NULL,
  `tempat_tinggal` varchar(200) NOT NULL,
  `telp` varchar(20) NOT NULL,
  `agama` varchar(20) NOT NULL,
  `gender` enum('L','P') NOT NULL,
  `tempat_lahir` varchar(50) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `username_siswa` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_siswa`
--

INSERT INTO `data_siswa` (`induk_siswa`, `nama_siswa`, `tempat_tinggal`, `telp`, `agama`, `gender`, `tempat_lahir`, `tanggal_lahir`, `username_siswa`, `password`) VALUES
('', '', '', '', '', '', '', '0000-00-00', '', ''),
('24545345345', 'osi', 'dfafdasfdsfds', '756756757', 'Islam', 'L', 'asdasd', '2017-07-04', 'osi', '135'),
('911', 'z', 'z', '911', 'Islam', '', 'z', '1970-01-01', 'as911', '9'),
('9292929292', 'hafizh', 'jsdoasdoasdaso', '0216326326323', 'Islam', 'L', 'malang', '2017-07-11', 'ambon', '123'),
('93', 'dsfsdfs', 'hjhkjjhgffsg', '67567567574', 'Lainnya', '', 'dhfhdfdfgdf', '2017-05-17', 'lailli', '138'),
('95', 'FireBolt111', 'asdjnhasdkbnasdki', '64361684', 'Islam', 'L', 'as mndjasdbkasd', '2017-05-17', 'admin2', '135'),
('96', 'asdsad', 'ghfghfghfghfdf', '46452', 'Islam', 'L', 'asgadgsdag', '2017-05-17', 'sfafdfa', '15'),
('97', 'askjda', 'oiadjasodjo', '4234234', 'Islam', 'P', 'kkdsdksfbkdskk', '0000-00-00', 'test', '158'),
('98', 'John Doe', 'Jl. Danau Maninjau G5 A17', '08965458255', 'Islam', '', 'Malang Sadja', '0000-00-00', 'john7', '158');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_siswa`
--
ALTER TABLE `data_siswa`
  ADD PRIMARY KEY (`induk_siswa`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
