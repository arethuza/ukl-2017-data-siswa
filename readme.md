# Installation
#### Download and Import the database
#### Akses website
```
localhost/project/login.php
```

## Preview
Website yang saya buat pada kelas 10 saat UKL tahun 2017

#### Login Page
<img src="https://cdn-images-1.medium.com/max/880/1*a5qNmxkoqaYGQgPAF84VsQ.png">

#### Register Page
<img src="https://cdn-images-1.medium.com/max/880/1*ouuH3ge8XcKKqBnWWMuWDg.png">

#### Dashboard
<img src="https://cdn-images-1.medium.com/max/880/1*Wp3RBYAt7GY_UA7hHrPsTw.png">

#### Edit Profile
<img src="https://cdn-images-1.medium.com/max/880/1*A233Xin5uJTU6B4FCRdg5g.png">

#### Delete Profile
<img src="https://cdn-images-1.medium.com/max/880/1*oLjU6_0Y5RE_IDI9msNj8w.png">