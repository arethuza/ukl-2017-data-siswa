<?php
	include '../connection.php';
	session_start();
	$edit=mysqli_query($connect, "select * from data_siswa where induk_siswa='$_SESSION[induk_siswa]'");
	$data=mysqli_fetch_array($edit);
	include 'atas.php';
?>

<style type="text/css">
	.container p {
		margin-left: 20px;
		margin-right: 80px;
	}
	input[type=text], input[type=password], textarea, input[type=number], input[type=date], select {
		margin-right: 40px;
	}
	.container h1 {
		margin-bottom: 60px;
	}

	.container input[type=submit] {
		margin: 40px 0 0 20px;
	}
</style>

<div class="container">
	<h1>Change Your Personal Information</h1><form action="in_profile.php" method="post">
	<table style="">
		<tr>
			<td><p style="margin-left: 20px;">Nomor Induk</p></td>
			<td><input type="text" disabled name="induk_siswa" value="<?php echo $data['induk_siswa']; ?>"></td>


			<td><p class="awah">Nama Lengkap</p></td>
			<td><input type="text" name="nama_siswa" value="<?php echo $data['nama_siswa']; ?>"></td>
		</tr>

		<tr>
			<td><p>Tempat Tinggal</p></td>
			<td><textarea name="tempat_tinggal"><?php echo $data['tempat_tinggal']; ?></textarea></td>
		

			<td><p class="awah">Tempat Lahir</p></td>
			<td><textarea name="tempat_lahir"><?php echo $data['tempat_lahir']; ?></textarea></td>
		</tr>

		<tr>
			<td><p>Agama</p></td>
			<td>
				<input type="text" name="agama" value="<?php echo $data['agama']; ?>">
			</td>
		

			<td><p class="awah">Gender</p></td>
			<td>
				<input type="text" name="gender" disabled value="<?php echo $data['gender']; ?>">
			</td>
		</tr>

		<tr>
			<td><p>Telp</p></td>
			<td><input type="number" name="telp" value="<?php echo $data['telp']; ?>"></td>
		

			<td><p class="awah">Tanggal Lahir</p></td>
			<td><input name="tanggal_lahir" type="date" value="<?php echo date('Y-m-d',strtotime($data['tanggal_lahir'])) ?>"></td>
		</tr>

		<tr>
			<td><p>Username</p></td>
			<td><input type="text" name="username_siswa" value="<?php echo $data['username_siswa']; ?>"></td>
		

			<td><p class="awah">Password</p></td>
			<td><input type="text" name="password" value="<?php echo $data['password']; ?>"></td>
		</tr>
	</table>
	<input type="submit" name="edit"></form>
</div>

<?php include 'bawah.php'; ?>