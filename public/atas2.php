<!DOCTYPE html>
<html>
<head>
	<title>Dashboard</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome.css">
</head>
<body>
	<div id="navbar">
		<i class="fa fa-chevron-left"></i><a id="row">KumaDex</a>
		<ul>
			<li>
				<i class="fa fa-user-circle-o"></i><a>User <?php session_start(); echo $_SESSION['nama_siswa']; ?></a>
			</li>
		</ul>
	</div>


	<div class="sBenteng">
		<div class="itemActive">
			<a class="list-group-item" href="#"><i class="fa fa-gear fa-fw" aria-hidden="true"></i>&nbsp; Tools</a>
		</div>

		<div class="item">
			<a class="list-group-item" href="#"><i class="fa fa-home fa-fw" aria-hidden="true"></i>&nbsp; Home</a>
		</div>

		<div class="item">
			<a class="list-group-item" href="edit_profile.php"><i class="fa fa-user-circle fa-fw" aria-hidden="true"></i>&nbsp; Edit Profile</a>
		</div>

		<div class="item">
			<a class="list-group-item" href="hapus.php"><i class="fa fa-close fa-fw" aria-hidden="true"></i>&nbsp; Delete Profile</a>
		</div>

		<div class="item">
			<a class="list-group-item" href="logout.php"><i class="fa fa-power-off fa-fw" aria-hidden="true"></i>&nbsp; Logout</a>
		</div>
		
	</div>
	